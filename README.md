# README #

Open the ZoomAndScroll.sln in Visual Studio 2015 or later and build

### What is this repository for? ###

The program demonstrates implementation of zoom cursor tool in WPF desktop application. The code contains also some basic elements of MVVM application for working with basic paginated document. The model is actually not present, the view models represent hardcoded data. 

ZoomAndScroll version 0.0.0.2

