﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZoomAndScroll
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static RoutedCommand Reset = new RoutedCommand("Reset", typeof(MainWindow));
        public static RoutedCommand Zoom = new RoutedCommand("Zoom", typeof(MainWindow));
        public static RoutedCommand About = new RoutedCommand("About", typeof(MainWindow));
        public static RoutedCommand Quits = new RoutedCommand("Exit", typeof(MainWindow));

        public MainWindow()
        {
            InitializeComponent();

            DataContext = new ViewModelDocument();

            CommandBindings.Add(new CommandBinding(Reset, CommandViewResetExecute, CommandViewResetCanExecute));
            CommandBindings.Add(new CommandBinding(Zoom, CommandViewZoomExecute, CommandViewZoomCanExecute));
            CommandBindings.Add(new CommandBinding(About, CommandViewAboutExecute, CommandViewAboutCanExecute));
            CommandBindings.Add(new CommandBinding(Quits, CommandViewQuitsExecute, CommandViewQuitsCanExecute));
        }

        #region CommandViewReset
        private void CommandViewResetExecute(object sender, ExecutedRoutedEventArgs e)
        {
            DocumentArea.ViewZoomSet(1.0);
            e.Handled = true;
        }

        private void CommandViewResetCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }
        #endregion

        #region CommandViewZoom
        private void CommandViewZoomExecute(object sender, ExecutedRoutedEventArgs e)
        {
            DocumentArea.ViewZoom(1.1);
            e.Handled = true;
        }

        private void CommandViewZoomCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }
        #endregion

        #region CommandViewAbout
        private void CommandViewAboutExecute(object sender, ExecutedRoutedEventArgs e)
        {
            AboutPopup.IsOpen = true;
            e.Handled = true;
        }

        private void CommandViewAboutCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }
        #endregion

        #region CommandViewQuits
        private void CommandViewQuitsExecute(object sender, ExecutedRoutedEventArgs e)
        {
            Application.Current.Shutdown();

            e.Handled = true;
        }

        private void CommandViewQuitsCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }
        #endregion
    }
}
