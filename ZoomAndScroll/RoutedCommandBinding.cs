﻿using System.Windows.Input;

namespace ZoomAndScroll
{
    // Can be used to bind static RoutedCommand or RoutedUICommand to implementation 
    // provided as another ICommand e.g. command from viewmodel. 
    // 
    public class RoutedCommandBinding : CommandBinding
    {
        public RoutedCommandBinding()
            : base()
        {
            Executed += CommandExecute;
            CanExecute += CommandCanExecute;
        }

        public RoutedCommandBinding(ICommand command, ICommand target)
            : base(command)
        {
            Target = target;
            Executed += CommandExecute;
            CanExecute += CommandCanExecute;
        }

        public void Finish()
        {
            Executed -= CommandExecute;
            CanExecute -= CommandCanExecute;
        }

        ICommand m_target = null;

        public ICommand Target
        {
            get { return m_target; }
            set { m_target = value; }
        }

        private void CommandExecute(object sender, ExecutedRoutedEventArgs e)
        {
            if (m_target != null)
            {
                m_target.Execute(e.Parameter);
                e.Handled = true;
            }
        }

        private void CommandCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (m_target != null)
            {
                e.CanExecute = m_target.CanExecute(e.Parameter);
                e.Handled = true;
            }
        }
    }
}
