﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ZoomAndScroll
{
    class RoutedCommands
    {
        // Document-level commands
        public static RoutedUICommand DocumentInvertColors = new RoutedUICommand("Invert Colors", "Invert Colors", typeof(RoutedCommands));
        // Page-level commands
        public static RoutedUICommand PageFlip = new RoutedUICommand("Flip Page", "Flip Page", typeof(RoutedCommands));
    }
}
