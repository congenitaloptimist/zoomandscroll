﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZoomAndScroll
{
    class ViewDocumentPage : Canvas
    {
        public ViewDocumentPage( )
        {
            MouseDown += ViewDocumentPage_MouseDown;
        }

        private void ViewDocumentPage_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            // FocusManager.SetFocusedElement(scope, this);
            Keyboard.Focus(this as IInputElement);
        }
    }
}
