﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZoomAndScroll
{
    class ViewDocumentPages : StackPanel
    {
        public ViewDocumentPages( )
            : base( )
        {
        }

        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
                if (e.Delta > 0)
                    ViewZoom(1.1);
                else
                    ViewZoom(1 / 1.1);
            }
        }

        public void ViewZoom(double factor)
        {
            System.Windows.Media.MatrixTransform t = null;
            if (LayoutTransform is System.Windows.Media.MatrixTransform)
                t = LayoutTransform.Clone() as System.Windows.Media.MatrixTransform;
            else
                t = new System.Windows.Media.MatrixTransform();

            double ScaleX = t.Matrix.M11 * factor;
            double ScaleY = t.Matrix.M22 * factor;

            t.Matrix = new System.Windows.Media.Matrix(ScaleX, 0, 0, ScaleY, t.Matrix.OffsetX, t.Matrix.OffsetY);

            LayoutTransform = t;
        }

        public void ViewZoomSet(double factor)
        {
            System.Windows.Media.MatrixTransform t = null;
            if (LayoutTransform is System.Windows.Media.MatrixTransform)
                t = LayoutTransform.Clone() as System.Windows.Media.MatrixTransform;
            else
                t = new System.Windows.Media.MatrixTransform();

            double ScaleX = factor * 96.0 / 72.0;
            double ScaleY = factor * 96.0 / 72.0;

            t.Matrix = new System.Windows.Media.Matrix(ScaleX, 0, 0, ScaleY, t.Matrix.OffsetX, t.Matrix.OffsetY);

            LayoutTransform = t;
        }

        public void ZoomToRectangle(ScrollViewer scroll, Rect rect)
        {
            if (scroll == null)
                return;

            // Can scroll if( scroll.ExtentWidth > scroll.ViewportWidth )
            // Maximum scroll.HorizontalOffset = scroll.ExtentWidth - scroll.ViewportWidth
            // Minimum scroll.HorizontalOffset = 0;

            // Can scroll if( scroll.ExtentHeight > scroll.ViewportHeight )
            // Maximum scroll.VerticalOffset = scroll.ExtentHeight - scroll.ViewportHeight
            // Minimum scroll.VerticalOffset = 0;

            // Viewable area size (viewport) is the size of ScrollContentPresenter control 
            // in ScrollViewer. ScrollContentPresenter is parent of CanvasStackPanel control 

            // Scrollable content size (extent) is the size of Border around document plus 
            // Border margins, with scale applied (96/72 by default)

            Panel zoom = this;
            if (zoom == null)
                return;

            // Scroll Extent is defined by Border element around this element

            double exw = scroll.ExtentWidth;
            double exh = scroll.ExtentHeight;

            double scalex = scroll.ViewportWidth / rect.Width;
            double scaley = scroll.ViewportHeight / rect.Height;

            double scale = Math.Min(scalex, scaley);

            System.Windows.Point test1 = new System.Windows.Point(50, 50);
            System.Windows.Point test2 = new System.Windows.Point(50, 50);

            test1 = this.TranslatePoint(test1, zoom);

            ViewZoomSet(scale);

            test2 = this.TranslatePoint(test2, zoom);

            // get current layout transform
            System.Windows.Media.MatrixTransform t = CurrentTransform();
            // point in coordinates of this window 
            System.Windows.Point origin = rect.TopLeft;
            // point in coordinates of scaling panel
            origin = this.TranslatePoint(origin, zoom);
            // apply current transformation (scale) to point
            origin = t.Transform(origin);

            scroll.ScrollToHorizontalOffset(origin.X);
            scroll.ScrollToVerticalOffset(origin.Y);

            // from http://stackoverflow.com/questions/3815558/wpf-zoom-and-scroll-to-selection-sample
        }

        System.Windows.Media.MatrixTransform CurrentTransform()
        {
            System.Windows.Media.MatrixTransform t = null;

            if (LayoutTransform is System.Windows.Media.MatrixTransform)
                t = LayoutTransform.Clone() as System.Windows.Media.MatrixTransform;
            else
                t = new System.Windows.Media.MatrixTransform();

            return t;
        }
    }
}
