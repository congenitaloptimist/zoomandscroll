﻿using System;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZoomAndScroll
{
    class ViewModelDocument : ViewModel
    {
        public ViewModelDocument( )
        {
            // populate for test
            Pages.Add(new ViewModelPage() { W = 595, H = 842 });
            Pages.Add(new ViewModelPage() { W = 595, H = 842 });
            Pages.Add(new ViewModelPage() { W = 595, H = 842 });

            // Add the commands to viewmodel bindings so the view can merge these into its bindings
            CommandBindings.Add(new RoutedCommandBinding(RoutedCommands.DocumentInvertColors, DocumentCommand));
        }

        public ViewModelPage Page
        {
            get { return m_page; }
            set { if(m_page != value) m_page = value; }
        }

        public ObservableCollection<ViewModelPage> Pages
        {
            get { return m_pages; }
        }

        readonly CommandBindingCollection m_CommandBindings = new CommandBindingCollection();
        public CommandBindingCollection CommandBindings
        {
            get
            {
                return m_CommandBindings;
            }
        }

        DelegateCommand m_command;
        public ICommand DocumentCommand
        {
            get
            {
                if (m_command == null)
                    m_command = new DelegateCommand(DocumentCommandExecuted, DocumentCommandCanExecute);
                return m_command;
            }
        }

        private void DocumentCommandExecuted(object parameter)
        {
            foreach(ViewModelPage page in Pages)
            {
                foreach(ViewModelObject element in page.Objects)
                {
                    System.Windows.Media.Color color = element.Color;
                    element.Color = System.Windows.Media.Color.FromScRgb(color.ScA, 1f - color.ScR, 1f - color.ScG, 1f - color.ScB);
                }
            }
        }

        private bool DocumentCommandCanExecute(object parameter)
        {
            return true;
        }

        ViewModelPage m_page;
        ObservableCollection<ViewModelPage> m_pages = new ObservableCollection<ViewModelPage>( );
    }
}
