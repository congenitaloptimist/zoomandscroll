﻿using System;
using System.Windows.Media;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZoomAndScroll
{
    class ViewModelObject : ViewModel
    {
        static private Random rnd = new Random();

        public ViewModelObject( )
        {
            Color = Color.FromArgb(255, (byte)rnd.Next(256), (byte)rnd.Next(256), (byte)rnd.Next(256));
            FillBrush = new SolidColorBrush(Color);
        }

        public double X
        {
            get { return m_X; }
            set { if (m_X != value) { m_X = value; OnPropertyChanged(); } }
        }

        public double Y
        {
            get { return m_Y; }
            set { if (m_Y != value) { m_Y = value; OnPropertyChanged(); } }
        }

        public double W
        {
            get { return m_W; }
            set { if (m_W != value) { m_W = value; OnPropertyChanged(); } }
        }

        public double H
        {
            get { return m_H; }
            set { if (m_H != value) { m_H = value; OnPropertyChanged(); } }
        }

        public Color Color
        {
            get { return m_color; }
            set { if (m_color != value) { m_color = value; FillBrush = new SolidColorBrush(value); OnPropertyChanged(); } }
        }

        public Brush FillBrush
        {
            get { return m_brush; }
            set { if (m_brush != value) { m_brush = value; OnPropertyChanged(); } }
        }

        Color m_color;
        Brush m_brush;

        double m_X;
        double m_Y;
        double m_W;
        double m_H;
    }
}
