﻿using System;
using System.Windows.Input;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZoomAndScroll
{
    class ViewModelPage : ViewModel
    {
        public ViewModelPage()
        {
            Objects.Add(new ViewModelObject() { X = 23, Y = 44, W = 100, H = 60 });
            Objects.Add(new ViewModelObject() { X = 73, Y = 144, W = 100, H = 60 });
            Objects.Add(new ViewModelObject() { X = 63, Y = 312, W = 100, H = 60 });
            Objects.Add(new ViewModelObject() { X = 26, Y = 455, W = 100, H = 60 });

            // Add the commands to viewmodel bindings so the view can merge these into its bindings
            CommandBindings.Add(new RoutedCommandBinding(RoutedCommands.PageFlip, Flip));
        }

        public ObservableCollection<ViewModelObject> Objects
        {
            get { return m_objects; }
        }

        public double W
        {
            get { return m_W; }
            set { if (m_W != value) { m_W = value; OnPropertyChanged(); } }
        }

        public double H
        {
            get { return m_H; }
            set { if (m_H != value) { m_H = value; OnPropertyChanged(); } }
        }

        readonly CommandBindingCollection m_CommandBindings = new CommandBindingCollection();
        public CommandBindingCollection CommandBindings
        {
            get
            {
                return m_CommandBindings;
            }
        }

        #region Command_Flip
        DelegateCommand m_Flip;
        public ICommand Flip
        {
            get
            {
                if (m_Flip == null)
                    m_Flip = new DelegateCommand(CommandFlipExecuted, CommandFlipCanExecute);
                return m_Flip;
            }
        }

        private void CommandFlipExecuted(object parameter)
        {
            double h = W;
            W = H;
            H = h;
        }

        private bool CommandFlipCanExecute(object parameter)
        {
            return true;
        }
        #endregion

        double m_W;
        double m_H;

        ObservableCollection<ViewModelObject> m_objects = new ObservableCollection<ViewModelObject>();
    }
}
