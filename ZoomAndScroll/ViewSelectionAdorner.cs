﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ZoomAndScroll
{
    class SelectionAdorner : System.Windows.Documents.Adorner
    {
        System.Windows.Point m_start;
        System.Windows.Point m_end;
        UIElement m_element;

        public SelectionAdorner(UIElement element)
            : base(element)
        {
            m_element = element;
            System.Windows.Documents.AdornerLayer layer = System.Windows.Documents.AdornerLayer.GetAdornerLayer(element);
            if (layer != null)
            {
                layer.Add(this);
            }
        }

        public SelectionAdorner(UIElement element, System.Windows.Point start)
            : base(element)
        {
            m_end = m_start = start;
            m_element = element;
            System.Windows.Documents.AdornerLayer layer = System.Windows.Documents.AdornerLayer.GetAdornerLayer(element);
            if (layer != null)
            {
                layer.Add(this);
            }
        }

        public void Finish()
        {
            System.Windows.Documents.AdornerLayer layer = System.Windows.Documents.AdornerLayer.GetAdornerLayer(m_element);
            if (layer != null)
            {
                layer.Remove(this);
            }
        }

        public System.Windows.Point Start
        {
            get { return m_start; }
            set { if (m_start != value) { m_start = value; Update(); } }
        }

        public System.Windows.Point End
        {
            get { return m_end; }
            set { if (m_end != value) { m_end = value; Update(); } }
        }

        void Update()
        {
            InvalidateVisual();
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            Rect rect = new Rect
            (
                Math.Min(m_start.X, m_end.X),
                Math.Min(m_start.Y, m_end.Y),
                Math.Abs(m_start.X - m_end.X),
                Math.Abs(m_start.Y - m_end.Y)
            );

            drawingContext.DrawRectangle(null, MakeDottedPen(), rect);
        }

        protected System.Windows.Media.Pen MakeDottedPen()
        {
            System.Windows.Media.Pen pen = new System.Windows.Media.Pen();
            pen.Brush = System.Windows.Media.Brushes.DarkBlue;
            System.Windows.Media.DashStyle dash = new System.Windows.Media.DashStyle();
            dash.Dashes = new System.Windows.Media.DoubleCollection(new double[] { 4, 2 });
            pen.DashStyle = dash;
            pen.DashCap = PenLineCap.Flat;

            if (m_element is Panel)
                pen.Thickness = 0.4;
            else
                pen.Thickness = 72.0 / 96.0;

            return pen;
        }
    }
}
